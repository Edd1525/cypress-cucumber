# Cypress Tests

Commands to execute scripts

`$ npm run tests` execute tests with chrome

`$ npm run tests:headless` execute test electron headless mode

`$ npm run generate:report` generate html cucumber reports
